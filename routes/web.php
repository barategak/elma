<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ControllerAuth;
use App\Http\Controllers\ControllerUser;
use App\Http\Controllers\ControllerAdmin;
use App\Http\Controllers\ControllerGejala;
use App\Http\Controllers\ControllerDiagnosis;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::match(['post', 'get'], '/login', [ControllerAuth::class, "login"]);

Route::middleware(["auth.custom"])->group(function() {
    Route::get('/', [ControllerAdmin::class, "dashboard"]);
    Route::get('/logout', [ControllerAuth::class, "logout"]);
    
    Route::group(['prefix' => 'user'], function()
    {
        Route::get('/', [ControllerUser::class, "index"]);
        Route::get('/create', [ControllerUser::class, "create"]);
        Route::post('/store', [ControllerUser::class, "store"]);
        Route::get('/edit/{id}', [ControllerUser::class, "edit"]);
        Route::post('/update/', [ControllerUser::class, "update"]);
        Route::get('/destroy/{id}', [ControllerUser::class, "destroy"]);

        Route::group(['prefix' => 'diagnosis'], function()
        {
            Route::get('/{id}', [ControllerDiagnosis::class, "index"]);
            Route::get('/destroy/{id}/{id_user}', [ControllerDiagnosis::class, "destroy"]);

            Route::group(['prefix' => 'gejala'], function()
            {
                Route::get('/{id}', [ControllerGejala::class, "index"]);
            });

        });
    });
});