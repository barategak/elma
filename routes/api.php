<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\ControllerApi;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'user'], function()
    {
        Route::post('/all', [ControllerApi::class, "all"]);
        Route::post('/detail', [ControllerApi::class, "show"]);
        Route::post('/get_diagnosis', [ControllerApi::class, "show_diagnosis"]);
        Route::post('/register', [ControllerApi::class, "register"]);
        Route::post('/login', [ControllerApi::class, "login"]);
        Route::post('/get_pertanyaan', [ControllerApi::class, "get_pertanyaan"]);
        Route::post('/submit_diagnosis', [ControllerApi::class, "submit_diagnosa"]);
        Route::post('/chatbot', [ControllerApi::class, "chatbot"]);
        Route::post('/connect', [ControllerApi::class, "connect"]);
    });
