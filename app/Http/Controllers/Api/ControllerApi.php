<?php

namespace App\Http\Controllers\Api;

use App\Models\User;
use App\Models\Gejala;
use App\Models\Diagnosis;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Hash;

class ControllerApi extends Controller
{

    private $base_url = "http://127.0.0.1:5000";

    function all(Request $request) {
        $data = User::all()->toJson();
        return $data;
    }

    function connect(Request $request) {
        $response_http = new Response();

        try {
            $response = ["status"=> $response_http->status()];
            
        }catch(\Exception $e) {
            $response = ["status"=> $response_http->status(), "data" => $e];
        }
        return $response;
    }


    function login(Request $request) {

        $response_http = new Response();

        $input= [
            "email" => $request->post("email"),
            "password" => $request->post("password")
        ];
        
        try {
            $login = User::where([
                ["email", "=",$input["email"]]
            ])->get();
            if(count($login) > 0) {
                if(Hash::check($input["password"], $login[0]->password)) {
                    unset($login[0]->password);
                    unset($login[0]->created_at);
                    unset($login[0]->updated_at);
                    $response = ["status"=> $response_http->status(), "data" => $login];
                }else {
                    $response = ["status"=> 400, "message" => "Email dan Kata Sandi tidak cocok"];
                }
            }else {
                $response = ["status"=> 400, "message" => "Email tidak terdaftar", "input" => $input];
            }   
            
        }catch(\Exception $e) {
            $response = ["status"=> $response_http->status(), "data" => $e->getMessage()];
        }
        return $response;
    }

    function show(Request $request) {
        $response_http = new Response();


        $input = [
            "id" => $request->post("id")
        ];

        try {
            $show = User::find($input);
            $response = ["status"=> $response_http->status(), "data" => $show];
        }catch(\Exception $e) {
            $response = ["status"=> $response_http->status(), "data" => $e];
        }
        return $response;

    }

    function show_diagnosis(Request $request) {
        $response_http = new Response();


        $input = [
            "id_user" => $request->post("id")
        ];

        try {
            $show = Diagnosis::where($input)->get();
            $data = [];
            foreach($show as $row) {
                $id_diagnosis = $row->id;
                $gejala = Gejala::where("id_diagnosis", $id_diagnosis)->get();
                array_push($data, ["gejala" => $gejala[0]->nama, "diagnosis" => $row->nama, "created_at" => $row->created_at ]);
            }
            $response = ["status"=> $response_http->status(), "data" => $data];
        }catch(\Exception $e) {
            $response = ["status"=> $response_http->status(), "data" => $e];
        }
        return $response;

    }



    function register(Request $request) {

        $response_http = new Response();

        $input= [
            "nama" => $request->post("nama"),
            "email" => $request->post("email"),
            "password" => bcrypt($request->post("password")),
            "tgl_lahir" => date("Y-m-d", strtotime($request->post("tgl_lahir") ) ),
            "no_hp" => $request->post("no_hp"),
            "pekerjaan" => $request->post("pekerjaan")
        ];

        try {
            $register = User::create($input);
            $response = ["status"=> $response_http->status(), "data" => $register];
        }catch(\Exception $e) {
            $response = ["status"=> $response_http->status(), "data" => $e];
        }
        return $response;
    }

    function get_pertanyaan(Request $request) {
        $endpoint = $this->base_url."/pertanyaan";
        $client = new \GuzzleHttp\Client();

        $response = $client->request('POST', $endpoint, ["form_params" => [
            "key" => $request->post("key")
        ]]);
        $statusCode = $response->getStatusCode();
        $content = json_decode($response->getBody(), true)["data"];
        $response_data = ["status" => $statusCode, "data" => $content];

        return json_encode($response_data);
    }

    // function submit_diagnosa(Request $request) {
    //     $endpoint = $this->base_url."/submit_diagnosa";
    //     $client = new \GuzzleHttp\Client();


    //     $response = $client->request('POST', $endpoint, ["form_params" => [
    //         "jawaban" => $request->post("jawaban")
    //     ]]);

        
    //     $statusCode = $response->getStatusCode();
    //     $content = json_decode($response->getBody(), true)["data"];

    //     if(count($content["diagnosis"]) == 0) {
    //         $create_diagnosis = Diagnosis::create([
    //             "nama" => "Tidak terdiagnosis",
    //             "id_user" => $request->post("id_user")
    //         ]);
    //         if(count($content["diagnosis"]) == 0) {
    //             $create_gejala = Gejala::create([
    //                 "nama" => "Tidak ada gejala",
    //                 "id_diagnosis" => $create_diagnosis->id
    //             ]);
    //         }else {
    //             $create_gejala = Gejala::create([
    //                 "nama" => implode(",", $content["gejala"]),
    //                 "id_diagnosis" => $create_diagnosis->id
    //             ]);
    //         }
            
    //     }else {
    //         $create_diagnosis = Diagnosis::create([
    //             "nama" => implode(",", $content["diagnosis"]),
    //             "id_user" => $request->post("id_user")
    //         ]);
    //         if(count($content["diagnosis"]) == 0) {
    //             $create_gejala = Gejala::create([
    //                 "nama" => "Tidak ada gejala",
    //                 "id_diagnosis" => $create_diagnosis->id
    //             ]);
    //         }else {
    //             $create_gejala = Gejala::create([
    //                 "nama" => implode(",", $content["gejala"]),
    //                 "id_diagnosis" => $create_diagnosis->id
    //             ]);
    //         }
    //     }

    //     $response_data = ["status" => $statusCode, "data" => $content];

    //     return json_encode($response_data);
    // }

    function submit_diagnosa(Request $request) {
        $endpoint = $this->base_url."/submit";
        $client = new \GuzzleHttp\Client();

        $response = $client->request('POST', $endpoint, ["form_params" => [
            "key" => $request->post("key"),
            "answer" => $request->post("jawaban")
        ]]);
        $statusCode = $response->getStatusCode();
        $content = json_decode($response->getBody(), true)["data"];
        $response_data = ["status" => $statusCode, "data" => $content];

        return json_encode($response_data);
    }


    function chatbot(Request $request) {
        $endpoint = $this->base_url."/chatbot";
        $client = new \GuzzleHttp\Client();


        $response = $client->request('POST', $endpoint, ["form_params" => [
            "key" => $request->post("key")
        ]]);

        
        $statusCode = $response->getStatusCode();
        $content = json_decode($response->getBody(), true)["data"];
        $response_data = ["status" => $statusCode, "data" => $content];

        return json_encode($response_data);
    }
}
