<?php

namespace App\Http\Controllers;
use Illuminate\Routing\Controller;

use Illuminate\Http\Request;

class ControllerAdmin extends Controller
{
    public function dashboard(Request $request) {
        return view("dashboard");
    }
}
