<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Gejala;
use App\Models\Diagnosis;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class ControllerGejala extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $gejala = Gejala::all();
        $diagnosa = Diagnosis::find($id);
        $user = User::find($diagnosa->id);
        return view("gejala.index", ["data_query" => $gejala, "id_user" => $id, "user" => $user, "diagnosis" => $diagnosa]);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
