<?php

namespace App\Http\Controllers;
use Illuminate\Routing\Controller;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class ControllerAuth extends Controller
{
    function login(Request $request) {
        if($request->has("username") && $request->has("password")) {
            if($request->post("username") == "admin" && $request->post("password") == "admin") {
                $request->session()->put("userAuth", "admin");
                return redirect("/");
            }else {
                return redirect("/login");
            }
        }else {
            return view("login");
        }
    }

    function logout(Request $request) {
        $request->session()->forget('userAuth');
        return redirect("/login");
    } 
}