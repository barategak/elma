<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Diagnosis;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class ControllerDiagnosis extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $diagnosa = Diagnosis::where("id_user", $id)->get();
        $user = User::find($id);
        return view("diagnosis.index", ["data_query" => $diagnosa, "id_user" => $id, "user" => $user]);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, $id_user)
    {
        $data = Diagnosis::find($id)->delete();
        return redirect("user/diagnosis/".$id_user)->with("status", "Sukses menghapus data!");
    }
}
