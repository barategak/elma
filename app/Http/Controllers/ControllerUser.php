<?php

namespace App\Http\Controllers;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use \Illuminate\Database\QueryException;


class ControllerUser extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = User::all();
        return view("user.index", ["data_query" => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("user.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = [
            "nama" => $request->post("nama"),
            "email" => $request->post("email"),
            "password" => $request->post("password"),
            "tgl_lahir" => $request->post("tgl_lahir"),
            "no_hp" => $request->post("no_hp"),
            "pekerjaan" => $request->post("pekerjaan")
        ];

        if(empty($input["nama"]) || empty($input["email"]) || empty($input["password"]) || empty($input["tgl_lahir"]) || empty($input["no_hp"]) || empty($input["pekerjaan"])) {
            return redirect("user/create")->with("status", "Form tidak boleh kosong!")->with("color", "danger");
        }else {
            $input["password"] = bcrypt($input["password"]);
            $user = User::create($input);
            return redirect("user/create")->with("status", "Sukses menambahkan data!")->with("color", "primary");
        }
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        return view("user.edit", ["data" => $user]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $id = $request->post("id");
        $input = [
            "nama" => $request->post("nama"),
            "email" => $request->post("email"),
            "password" => $request->post("password"),
            "tgl_lahir" => $request->post("tgl_lahir"),
            "no_hp" => $request->post("no_hp"),
            "pekerjaan" => $request->post("pekerjaan")
        ];

        if(empty($input["nama"]) || empty($input["email"]) || empty($input["password"]) || empty($input["tgl_lahir"]) || empty($input["no_hp"]) || empty($input["pekerjaan"])) {
            return redirect("user/edit/".$id)->with("status", "Form tidak boleh kosong!")->with("color", "danger");
        }else {
            $input["password"] = bcrypt($input["password"]);
            $user = User::find($id)->update($input);
            return redirect("user/edit/".$id)->with("status", "Sukses update data!")->with("color", "primary");
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id)->delete();
        return redirect("user")->with("status", "Sukses hapus data!")->with("color", "primary");
    }
}
