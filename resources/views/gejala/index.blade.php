@extends('template')


@section('css')
<link rel="stylesheet" href="/assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="/assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
<link rel="stylesheet" href="/assets/plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
@endsection

@section('js')
<script src="/assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="/assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="/assets/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="/assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="/assets/plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="/assets/plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="/assets/plugins/jszip/jszip.min.js"></script>
<script src="/assets/plugins/pdfmake/pdfmake.min.js"></script>
<script src="/assets/plugins/pdfmake/vfs_fonts.js"></script>
<script src="/assets/plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="/assets/plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="/assets/plugins/datatables-buttons/js/buttons.colVis.min.js"></script>

<script>
  $(function () {
    $("#example1").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false,
      "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });
</script>
@endsection




@section('content_header')

<section class="content-header">
    <div class="container-fluid">
    <div class="row mb-2">
        <div class="col-sm-6">
        <h1>Data Diagnosis</h1>
        </div>
        <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
            <!-- <li class="breadcrumb-item"><a href="#">Home</a></li> -->
            <li class="breadcrumb-item"><a href="{{ url('/user') }}">Data User</a></li>
            <li class="breadcrumb-item"><a href="{{ url('/user/diagnosis/') }}/{{ $user->id }}">Data Diagnosis</a></li>
            <li class="breadcrumb-item active">Data Gejala</li>
        </ol>
        </div>
    </div>
    </div>
</section>

@endsection

@section('content')

<section class="content">

@if (session('status'))
    <div class="card card-primary">
        <div class="card-header">
        <h3 class="card-title">Status</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form method="post" action="{{ url('/user/store') }}">
        @csrf
        <div class="card-body">
            {{ session('status') }}
        </div>
        <!-- /.card-body -->

        </form>
    </div>
    @endif

    <div class="card">
        <div class="card-header">
            <div style="float:left">
                <h3 class="card-title">Tabel Data Gejala Dengan Diagnosis : {{$diagnosis->nama}}, User :  {{$user->nama}}</h3>
            </div>
            <div style="clear:both"></div>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <table id="example1" class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th>Diagnosa</th>
                        <th>Waktu Diagnosa</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                @foreach ($data_query as $data)
                    <tr>
                        <td>{{ $data->nama }}</td>
                        <td>{{ $data->created_at }}</td>
                        <td>
                            <a href="{{ url('/') }}/{{$data->id}}" class="btn btn-primary btn-sm">Delete</a>
                        </td>
                    </tr>
                @endforeach
                    
                </tbody>
        </table>
    </div>

</section>

@endsection
