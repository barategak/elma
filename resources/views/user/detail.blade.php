@extends('template')


@section('css')

@endsection

@section('js')

<script src="/assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="/assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="/assets/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="/assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="/assets/plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="/assets/plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="/assets/plugins/jszip/jszip.min.js"></script>
<script src="/assets/plugins/pdfmake/pdfmake.min.js"></script>
<script src="/assets/plugins/pdfmake/vfs_fonts.js"></script>
<script src="/assets/plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="/assets/plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="/assets/plugins/datatables-buttons/js/buttons.colVis.min.js"></script>

<script>
  $(function () {
    $('#example1').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
    $('#example3').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });
</script>

@endsection






@section('content_header')

<section class="content-header">
    <div class="container-fluid">
    <div class="row mb-2">
        <div class="col-sm-6">
        <h1>Detail Data</h1>
        </div>
        <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{ url('/user') }}">Data User</a></li>
            <li class="breadcrumb-item active">Detail Data</li>
        </ol>
        </div>
    </div>
    </div>
</section>

@endsection

@section('content')

<section class="content">

    <div class="card card-primary">
        <div class="card-header">
        <h3 class="card-title">Detail Data</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        @csrf
        <div class="card-body">
            <div class="row">
                <div class="col-lg-6">
                    <p>NIM : {{ $data[0]->nim }}</p>
                    <p>Nama : {{ $data[0]->nama }}</p>
                    <p>Tempat Lahir : {{ $data[0]->tempat_lahir }}</p>
                    <p>Tanggal Lahir : {{ date('d-m-Y', strtotime($data[0]->tanggal_lahir)); }}</p>
                    <p>Alamat : {{ $data[0]->alamat }}</p>
                </div>
                <div class="col-lg-6">
                    <p>Jenis Kelamin : 
                        @if ($data[0]->jenis_kelamin == 0) 
                            Laki Laki
                        @else
                            Perempuan
                        @endif
                    </p>
                    <p>Energi : {{ $data[0]->energi }}</p>
                    <p>Score : {{ $data[0]->score }}</p>
                    <p>XP : {{ $data[0]->xp }}</p>
                </div>
            </div>
        </div>
        <!-- /.card-body -->
    </div>

    <div class="card card-primary">
        <div class="card-header">
        <h3 class="card-title">Riwayat Level Yang Dikerjakan</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        @csrf
        <div class="card-body">
            <table id="example1" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>Nama Level</th>
                            <th>Jumlah Bintang</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach ($data_level as $data)
                        <tr>
                            <td>{{ $data->nama }}</td>
                            <td>{{ $data->jumlah }}</td>
                        </tr>
                    @endforeach
                        
                    </tbody>
            </table>
        </div>
        <!-- /.card-body -->
    </div>

    <div class="card card-primary">
        <div class="card-header">
        <h3 class="card-title">Riwayat Rank Yang Dicapai</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        @csrf
        <div class="card-body">
            <table id="example2" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>Nama Rank</th>
                            <th>Tanggal</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach ($data_rank as $data)
                        <tr>
                            <td>{{ $data->nama }}</td>
                            <td>{{ date("d-m-Y", strtotime($data->date)) }}</td>
                        </tr>
                    @endforeach
                        
                    </tbody>
            </table>
        </div>
        <!-- /.card-body -->
    </div>
    
    <div class="card card-primary">
        <div class="card-header">
        <h3 class="card-title">Riwayat Daily Mission Yang Diselesaikan</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        @csrf
        <div class="card-body">
            <table id="example2" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>Nama Misi</th>
                            <th>Tanggal</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach ($data_mission as $data)
                        <tr>
                            <td>
                                @if ($data->id_mission == 1)
                                    Login
                                @elseif ($data->id_mission == 2)
                                    Menyelesaikan 3 level
                                @elseif ($data->id_mission == 3)
                                    Menyelesaikan 4 level
                                @elseif ($data->id_mission == 4)
                                    Menyelesaikan 5 level
                                @endif
                            </td>
                            <td>{{ date("d-m-Y", strtotime($data->date)) }}</td>
                        </tr>
                    @endforeach
                        
                    </tbody>
            </table>
        </div>
        <!-- /.card-body -->
    </div>

    <div class="card card-primary">
        <div class="card-header">
        <h3 class="card-title">Riwayat Achivement Yang Dicapai</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        @csrf
        <div class="card-body">
            <table id="example2" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>Nama Achievement</th>
                            <th>Tanggal</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach ($data_achievement as $data)
                        <tr>
                            <td>
                                @if ($data->id == 1)
                                Mendapatkan 1 Bintang
                                @elseif ($data->id == 2)
                                Mendapatkan 5 Bintang
                                @elseif ($data->id == 3)
                                Mendapatkan 8 Bintang
                                @elseif ($data->id == 4)
                                Mendapatkan 11 Bintang
                                @elseif ($data->id == 5)
                                Login hari ke-1
                                @elseif ($data->id == 6)
                                Login hari ke-7
                                @elseif ($data->id == 7)
                                Login hari ke-15
                                @elseif ($data->id == 8)
                                Login hari ke-30
                                @elseif ($data->id == 9)
                                Mencapai level exp 5
                                @elseif ($data->id == 10)
                                Mencapai level exp 10
                                @elseif ($data->id == 11)
                                Mencapai level exp 15
                                @elseif ($data->id == 12)
                                Mencapai level exp 20
                                @elseif ($data->id == 13)
                                Mencapai level exp 25
                                @elseif ($data->id == 14)
                                Mendapat skor 100
                                @elseif ($data->id == 15)
                                Mendapat skor 500
                                @elseif ($data->id == 16)
                                Mendapat skor 1200
                                @elseif ($data->id == 17)
                                Mendapat skor 2000
                                @elseif ($data->id == 18)
                                Menyelesaikan 3 achievement
                                @elseif ($data->id == 19)
                                Menyelesaikan 7 achievement
                                @elseif ($data->id == 20)
                                Menyelesaikan 14 achievement
                                @elseif ($data->id == 21)
                                Menyelesaikan 20 achievement
                                @endif
                            </td>
                            <td>{{ date("d-m-Y", strtotime($data->date)) }}</td>
                        </tr>
                    @endforeach
                        
                    </tbody>
            </table>
        </div>
        <!-- /.card-body -->
    </div>

    <div class="card card-primary">
        <div class="card-header">
        <h3 class="card-title">Log User</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        @csrf
        <div class="card-body">
            <table id="example3" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>Log</th>
                            <th>Tanggal</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach ($data_log as $data)
                        <tr>
                            <td>{{ $data->log }}</td>
                            <td>{{ date("d-m-Y", strtotime($data->date)) }}</td>
                        </tr>
                    @endforeach
                        
                    </tbody>
            </table>
        </div>
        <!-- /.card-body -->
    </div>

</section>

@endsection
