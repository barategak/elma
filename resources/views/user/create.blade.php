@extends('template')


@section('css')

@endsection

@section('js')

@endsection




@section('content_header')

<section class="content-header">
    <div class="container-fluid">
    <div class="row mb-2">
        <div class="col-sm-6">
        <h1>Tambah User</h1>
        </div>
        <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{ url('/user') }}">Data User</a></li>
            <li class="breadcrumb-item active">Tambah User</li>
        </ol>
        </div>
    </div>
    </div>
</section>

@endsection

@section('content')

<section class="content">
    @if (session('status'))
    <div class="card card-{{ session('color') }}">
        <div class="card-header">
        <h3 class="card-title">Status</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form method="post" action="{{ url('/user/store') }}">
        @csrf
        <div class="card-body">
            {{ session('status') }}
        </div>
        <!-- /.card-body -->

        </form>
    </div>
    @endif

    <div class="card card-primary">
        <div class="card-header">
        <h3 class="card-title">Form Tambah User</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form method="post" action="{{ url('/user/store') }}">
        @csrf
        <div class="card-body">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="form-group">
                <label for="exampleInputEmail1">Nama</label>
                <input type="text" name="nama" class="form-control"  placeholder="Masukkan Nama">
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Email</label>
                <input type="text" name="email" class="form-control" placeholder="Masukkan Email">
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Password</label>
                <input type="password" name="password" class="form-control"  placeholder="Masukkan Password">
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Tanggal Lahir</label>
                <input type="date" class="form-control" name="tgl_lahir" />
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Nomer Handphone</label>
                <input type="text" name="no_hp" class="form-control"  placeholder="Masukkan Nomer Handphone">
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Pekerjaan</label>
                <select name="pekerjaan" class="form-control">
                    <option value="Bekerja">Bekerja</option>
                    <option value="Pelajar">Pelajar</option>
                </select>
            </div>
        </div>
        <!-- /.card-body -->

        <div class="card-footer">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
        </form>
    </div>

</section>

@endsection
